# Prova
## _Linguagem de Programação III _

Prova usando node.js

## Features

- CRUD de pessoas
- CRUD de salas



## Instalação

Instale as dependências e devDependencies e inicie o servidor.

```sh
cd linguagem3
npm install
npm run dev
```

## License

MIT




